package com.company.shaaira.hrevent.model.database.dao

import android.arch.persistence.room.*
import com.company.shaaira.hrevent.commondata.ParticipantModel
import io.reactivex.Single

@Dao
interface ParticipantsDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertParticipant(participant: ParticipantModel): Long

    @Query("SELECT * FROM participantmodel WHERE eventId = :eventId")
    fun getParticipantsForEvent(eventId: Int): Single<List<ParticipantModel>>

    @Query("SELECT COUNT(*) FROM participantmodel WHERE isComing = 1 and eventId = :eventId")
    fun getParticipantsCameCount(eventId: Int): Single<Int>

    @Query("SELECT COUNT(*) FROM participantmodel WHERE eventId = :eventId")
    fun getParticipantsCount(eventId: Int): Single<Int>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertParticipants(events: List<ParticipantModel>)

    @Update
    fun updateParticipant(participant: ParticipantModel)
}