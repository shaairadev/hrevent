package com.company.shaaira.hrevent.model.datasource.local

import com.company.shaaira.hrevent.commondata.EventModel
import com.company.shaaira.hrevent.commondata.ParticipantModel
import io.reactivex.Single

interface EventsLocalDatasource {
    fun getEventsList(): Single<List<EventModel>>
    fun saveEventsList(events: List<EventModel>)
    fun saveParticipantsList(participants: List<ParticipantModel>)
}