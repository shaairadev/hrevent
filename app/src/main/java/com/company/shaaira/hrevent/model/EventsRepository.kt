package com.company.shaaira.hrevent.model

import com.company.shaaira.hrevent.commondata.EventModel
import com.company.shaaira.hrevent.model.datasource.local.EventsLocalDatasource
import com.company.shaaira.hrevent.model.datasource.local.EventsRoomModule
import com.company.shaaira.hrevent.model.datasource.network.EventsNetworkDatasource
import com.company.shaaira.hrevent.model.datasource.network.EventsNetworkModule
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class EventsRepository {

    private val networkDataSource: EventsNetworkDatasource = EventsNetworkModule()

    private val localDatasource: EventsLocalDatasource = EventsRoomModule()




    fun loadEventsList(): Single<List<EventModel>> {


        return networkDataSource.loadEvents()
                .flatMap { data ->

                    data.forEach { event ->
                        networkDataSource.loadParticipants(event.id)
                                .map {
                                    localDatasource.saveParticipantsList(it)
                                }
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io())
                                .subscribe()
                    }

                    localDatasource.saveEventsList(data)
                    localDatasource.getEventsList()
                }
                .onErrorResumeNext(localDatasource.getEventsList())
    }
}