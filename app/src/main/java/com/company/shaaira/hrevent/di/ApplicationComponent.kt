package com.company.shaaira.hrevent.di

import com.company.shaaira.hrevent.model.datasource.local.EventsRoomModule
import com.company.shaaira.hrevent.model.datasource.local.ParticipantsRoomModule
import com.company.shaaira.hrevent.model.datasource.network.EventsNetworkModule
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [ApplicationModule::class, RetrofitModule::class, RoomModule::class])
interface ApplicationComponent {
    fun inject(eventsNetworkModule: EventsNetworkModule)
    fun inject(eventsRoomModule: EventsRoomModule)
    fun inject(participantsRoomModule: ParticipantsRoomModule) {
    }
}