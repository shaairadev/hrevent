package com.company.shaaira.hrevent.model.dto

data class CityDTO(
        val icon: String,
        val id: Int,
        val isActive: Boolean,
        val nameEng: String,
        val nameRus: String
)