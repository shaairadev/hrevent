package com.company.shaaira.hrevent.presentation.eventsview.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.company.shaaira.hrevent.R
import com.company.shaaira.hrevent.commondata.EventModel
import kotlinx.android.synthetic.main.event_row.view.*
import java.text.SimpleDateFormat
import java.util.*

class EventsAdapter(private val eventModels: ArrayList<EventModel>,
                    private val eventClickListener : EventClickListener) :
        RecyclerView.Adapter<EventsAdapter.EventViewHolder>() {

    fun updateAdapterData(eventModels: List<EventModel>) {
        this.eventModels.clear()
        this.eventModels.addAll(eventModels)

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): EventViewHolder {
        return EventViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.event_row, viewGroup, false))
    }

    override fun onBindViewHolder(eventViewHolder: EventViewHolder, @SuppressLint("RecyclerView") position: Int) {
        eventViewHolder.bind(eventClickListener, eventModels[position])
    }

    override fun getItemCount(): Int {
        return eventModels.size
    }


    class EventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        companion object {
            const val DATE_FORMAT = "dd MMMM yyyy г."
            const val TIME_FORMAT = "HH:SS"
            const val LANGUAGE = "ru"
            const val BASE_URL = "https://beta-team.cft.ru/"
        }

    fun bind(eventClickListener: EventsAdapter.EventClickListener, event: EventModel) {
            val dateFormat = SimpleDateFormat(DATE_FORMAT, Locale(LANGUAGE))
            val timeFormat = SimpleDateFormat(TIME_FORMAT, Locale(LANGUAGE))

            itemView.eventStartDateTxt.text = dateFormat.format(event.beginDate)
            itemView.eventTitleTxt.text = event.title
            itemView.eventContentTxt.text = event.description
            itemView.eventStartTimeTxt.text = timeFormat.format(event.beginDate)

            val participantsString = "${event.cameMember} / ${event.totalMemberAmount}"
            itemView.eventParticipantsPassedTxt.text = participantsString



            Glide.with(itemView).load(BASE_URL + event.cardImage).into(itemView.eventImageView)
            itemView.setOnClickListener { eventClickListener.onClick(event.id, event.title) }
        }

}

    interface EventClickListener {
        fun onClick(eventId: Int, eventTitle: String)
    }
}