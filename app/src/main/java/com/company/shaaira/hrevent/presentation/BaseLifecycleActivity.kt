package com.company.shaaira.hrevent.presentation

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.company.shaaira.hrevent.lifecyclemodules.AcitivityToolbarListener
import com.company.shaaira.hrevent.lifecyclemodules.ActivityResultListener
import com.company.shaaira.hrevent.lifecyclemodules.LifecycleListener

abstract class BaseLifecycleActivity : AppCompatActivity() {
    private val lifeCycleDependants = ArrayList<LifecycleListener>()
    private val activityResultListeners = ArrayList<ActivityResultListener>()
    private val activityToolbarListener = ArrayList<AcitivityToolbarListener>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        for (listener in lifeCycleDependants) {
            listener.onCreate(savedInstanceState)
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        for (listener in lifeCycleDependants) {
            listener.onDestroy()
        }
    }

    override fun onStart() {
        super.onStart()

        for (listener in lifeCycleDependants) {
            listener.onStart()
        }
    }

    override fun onStop() {
        super.onStop()

        for (listener in lifeCycleDependants) {
            listener.onStop()
        }
    }

    override fun onResume() {
        super.onResume()

        for (listener in lifeCycleDependants) {
            listener.onResume()
        }
    }

    override fun onPause() {
        super.onPause()

        for (listener in lifeCycleDependants) {
            listener.onPause()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        for (listener in activityToolbarListener) {
            listener.onOptionsItemSelected(item)
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        for (listener in activityToolbarListener) {
            listener.onCreateOptionsMenu(menu)
        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        for (listener in activityResultListeners) {
            listener.onActivityResult(requestCode, resultCode, data)
        }
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        for (listener in lifeCycleDependants) {
            listener.onSaveInstanceState(outState)
        }
    }


    protected fun addLifecycleDependency(listener: LifecycleListener) {
        lifeCycleDependants.add(listener)
    }

    protected fun addActivityResultListener(listener: ActivityResultListener) {
        activityResultListeners.add(listener)
    }

    protected fun addActivityToolbarListener(listener: AcitivityToolbarListener)
    {
        activityToolbarListener.add(listener)
    }
}