package com.company.shaaira.hrevent.presentation.eventsview

import android.os.Bundle
import com.company.shaaira.hrevent.R
import com.company.shaaira.hrevent.presentation.BaseLifecycleActivity
import com.company.shaaira.hrevent.presentation.eventsview.adapters.EventsAdapter
import com.company.shaaira.hrevent.presentation.participantsview.ParticipantsActivity

class EventsActivity : BaseLifecycleActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_event)
        val eventView = EventsView(this,
                object : EventsAdapter.EventClickListener {
                    override fun onClick(eventId: Int, eventTitle: String) {
                        startActivity(ParticipantsActivity
                                .makeStartIntent(this@EventsActivity, eventId, eventTitle))
                    }
                })
        val eventPresenter = EventsPresenter(eventView)

        addLifecycleDependency(eventView)
        addLifecycleDependency(eventPresenter)
        addActivityToolbarListener(eventView)

        super.onCreate(savedInstanceState)
    }
}
