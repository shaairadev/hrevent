package com.company.shaaira.hrevent.model.dto

data class EventDTO(
        val cardImage: String,
        val cityDTOS: List<CityDTO>,
        val date: DateDTO,
        val description: String,
        val eventFormat: String,
        val eventFormatEng: String,
        val format: Int,
        val iconStatus: String,
        val id: Int,
        val status: Int,
        val title: String
)



