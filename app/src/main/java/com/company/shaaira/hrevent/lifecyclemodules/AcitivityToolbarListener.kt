package com.company.shaaira.hrevent.lifecyclemodules

import android.view.Menu
import android.view.MenuItem

interface AcitivityToolbarListener {

    fun onOptionsItemSelected(item : MenuItem?)

    fun onCreateOptionsMenu(menu : Menu?)
}