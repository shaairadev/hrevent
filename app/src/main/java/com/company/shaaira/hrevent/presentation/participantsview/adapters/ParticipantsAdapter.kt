package com.company.shaaira.hrevent.presentation.participantsview.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.company.shaaira.hrevent.R
import com.company.shaaira.hrevent.commondata.ParticipantModel
import kotlinx.android.synthetic.main.participants_row.view.*

class ParticipantsAdapter(private val participantsList: ArrayList<ParticipantModel>,
                          private val participantClickListener: ParticipantClickListener) :
        RecyclerView.Adapter<ParticipantsAdapter.ParticipantViewHolder>() {

    fun updateAdapterData(participantsList: List<ParticipantModel>) {
        this.participantsList.clear()
        this.participantsList.addAll(participantsList)

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): ParticipantViewHolder {
        return ParticipantViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.participants_row, viewGroup, false))
    }

    override fun onBindViewHolder(participantViewHolder: ParticipantViewHolder, @SuppressLint("RecyclerView") position: Int) {
        participantViewHolder.itemView.participants_check_box.isChecked = participantsList[position].isComing
        participantViewHolder.itemView.participants_check_box.setOnCheckedChangeListener{listener, checkValue->
                participantsList[participantViewHolder.adapterPosition].isComing = checkValue
            //TODO: Notify of user state change
        }
        participantViewHolder.bind(participantClickListener, participantsList[position])
    }

    override fun getItemCount(): Int {
        return participantsList.size
    }


    class ParticipantViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(participantClickListener: ParticipantsAdapter.ParticipantClickListener, participant: ParticipantModel) {
            itemView.participants_id_txt.text = participant.id.toString()
            itemView.participants_name_txt.text = "${participant.firstName} ${participant.lastName}"
            itemView.setOnClickListener { participantClickListener.onClick(participantId = participant.id) }
        }
    }


    interface ParticipantClickListener {
        fun onClick(participantId: Int)
    }
}