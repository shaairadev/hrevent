package com.company.shaaira.hrevent.model.dto

data class DateDTO(
        val end: String,
        val start: String
)