package com.company.shaaira.hrevent.model.dto

data class ParticipantDTO(
        val addition: String,
        val agreedByManager: String,
        val city: String,
        val company: String,
        val email: String,
        val firstName: String,
        val id: Int,
        val lastName: String,
        val patronymic: String,
        val phone: String,
        val position: String,
        val registeredDate: String
)