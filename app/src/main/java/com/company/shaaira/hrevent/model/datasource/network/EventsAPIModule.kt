package com.company.shaaira.hrevent.model.datasource.network

import com.company.shaaira.hrevent.commondata.EventModel
import com.company.shaaira.hrevent.commondata.ParticipantModel
import com.company.shaaira.hrevent.extensions.toEventEntity
import com.company.shaaira.hrevent.extensions.toParticipantEntity
import com.company.shaaira.hrevent.model.CFTAPI
import io.reactivex.Observable
import io.reactivex.Single

class EventsAPIModule(val api: CFTAPI) {
    fun getEventsCard(): Single<List<EventModel>> {
        return api.getEventsCard().flatMap {
            Observable.fromIterable(it)
                    .map { eventDTO -> eventDTO.toEventEntity() }
                    .toList()
        }
    }

    fun getParticipants(eventId: Int): Single<List<ParticipantModel>> {
        return api.getParticipants(eventId).flatMap {
            Observable.fromIterable(it)
                    .map { participantDTO -> participantDTO.toParticipantEntity(eventId) }
                    .toList()
        }
    }
}