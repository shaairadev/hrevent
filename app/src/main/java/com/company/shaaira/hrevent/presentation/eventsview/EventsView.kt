package com.company.shaaira.hrevent.presentation.eventsview

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import com.company.shaaira.hrevent.R
import com.company.shaaira.hrevent.commondata.EventModel
import com.company.shaaira.hrevent.lifecyclemodules.AcitivityToolbarListener
import com.company.shaaira.hrevent.lifecyclemodules.LifecycleListenerImpl
import com.company.shaaira.hrevent.presentation.eventsview.adapters.EventsAdapter
import kotlinx.android.synthetic.main.activity_event.*
import java.util.*

class EventsView(private val activity: AppCompatActivity,
                 private val clickListener: EventsAdapter.EventClickListener) :
        AcitivityToolbarListener,
        LifecycleListenerImpl() {

    private val listAdapter: EventsAdapter
    private lateinit var refreshCallback: RefreshCallback
    private lateinit var topToolbar: Toolbar
    private lateinit var refreshLayout: SwipeRefreshLayout

    init {
        listAdapter = initRecyclerView()
    }

    private fun initRecyclerView(): EventsAdapter {
        val recyclerView: RecyclerView = activity.event_view_recyclerView
        val adapter = EventsAdapter(ArrayList(), clickListener)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(activity)
        return adapter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        initToolbar()
        initSwipeRefreshLayout()
    }

    private fun initToolbar() {
        topToolbar = activity.findViewById(R.id.event_view_menu)
        setToolbarTitle(activity.getString(R.string.event))
        activity.setSupportActionBar(topToolbar)
    }

    private fun initSwipeRefreshLayout() {
        refreshLayout = activity.swipe_layout
        refreshLayout.setOnRefreshListener {
            refreshCallback.onRefresh()
        }
    }

    private fun setToolbarTitle(title: String?) =
            activity.runOnUiThread { topToolbar.title = title }

    internal fun setRefreshAnimation(state: Boolean) {
        if (refreshLayout.isRefreshing != state) {
            activity.runOnUiThread { refreshLayout.isRefreshing = state }
        }
    }

    fun updateEventList(eventModels: List<EventModel>) {
        activity.runOnUiThread { listAdapter.updateAdapterData(eventModels) }
    }

    fun setRefreshCallback(callback: RefreshCallback) {
        refreshCallback = callback
    }

    override fun onOptionsItemSelected(item: MenuItem?) {
        if (item != null && item.itemId == R.id.menu_refresh) {
            refreshCallback.onRefresh()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?) {

    }

    interface RefreshCallback {
        fun onRefresh()
    }
}