package com.company.shaaira.hrevent.model

import com.company.shaaira.hrevent.model.dto.EventDTO
import com.company.shaaira.hrevent.model.dto.ParticipantDTO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface CFTAPI {
        @GET("/api/v1/Events/registration")
        fun getEventsCard(): Single<List<EventDTO>>

        @GET("/api/v1/Registration/members/event/{eventId}?token=cftteamtest2018")
        fun getParticipants(@Path("eventId") eventId: Int): Single<List<ParticipantDTO>>

}