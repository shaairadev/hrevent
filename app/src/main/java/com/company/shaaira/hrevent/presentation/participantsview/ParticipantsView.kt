package com.company.shaaira.hrevent.presentation.participantsview

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import com.company.shaaira.hrevent.R
import com.company.shaaira.hrevent.commondata.ParticipantModel
import com.company.shaaira.hrevent.lifecyclemodules.LifecycleListenerImpl
import com.company.shaaira.hrevent.presentation.participantsview.adapters.ParticipantsAdapter
import kotlinx.android.synthetic.main.activity_participants.*

class ParticipantsView(private val activity: AppCompatActivity,
                       private val clickListener: ParticipantsAdapter.ParticipantClickListener) :
        LifecycleListenerImpl() {

    private val listAdapter: ParticipantsAdapter
    private lateinit var topToolbar: Toolbar

    init {
        listAdapter = initRecyclerView()
    }

    private fun initRecyclerView(): ParticipantsAdapter {
        val recyclerView: RecyclerView = activity.participants_view_recyclerView
        val adapter = ParticipantsAdapter(ArrayList(), clickListener)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(activity)
        return adapter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        initToolbar()
    }

    private fun initToolbar() {
        topToolbar = activity.findViewById(R.id.participants_view_menu)
        setToolbarTitle(activity.getString(R.string.participants))
        activity.setSupportActionBar(topToolbar)
    }

    private fun setToolbarTitle(title: String?) =
            activity.runOnUiThread { topToolbar.title = title }

    fun updateParticipantsList(participantModels: List<ParticipantModel>) {
        activity.runOnUiThread { listAdapter.updateAdapterData(participantModels) }
    }
}