package com.company.shaaira.hrevent.presentation.participantsview

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.company.shaaira.hrevent.R
import com.company.shaaira.hrevent.presentation.BaseLifecycleActivity
import com.company.shaaira.hrevent.presentation.participantsview.adapters.ParticipantsAdapter

class ParticipantsActivity : BaseLifecycleActivity() {

    companion object {
        private const val EXTRA_EVENT_TITLE = "title_event_extra"
        private const val EXTRA_EVENT_ID = "id_event_extra"

        fun makeStartIntent(context: Context, eventId: Int, eventTitle: String): Intent {
            val startIntent = Intent(context, ParticipantsActivity::class.java)
            startIntent.apply {
                putExtra(EXTRA_EVENT_ID, eventId)
                putExtra(EXTRA_EVENT_TITLE, eventTitle)
            }
            return startIntent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_participants)
        val participantsView = ParticipantsView(this,
                object : ParticipantsAdapter.ParticipantClickListener {
                    override fun onClick(participantId: Int) {
                        //TODO: Open participant card
                    }
                })
        val participantsPresenter = ParticipantsPresenter(participantsView,intent.getIntExtra(EXTRA_EVENT_ID,0))
        addLifecycleDependency(participantsView)
        addLifecycleDependency(participantsPresenter)

        super.onCreate(savedInstanceState)
    }
}
