package com.company.shaaira.hrevent.lifecyclemodules

import android.os.Bundle

interface LifecycleListener {
    fun onCreate(savedInstanceState: Bundle?)

    fun onDestroy()

    fun onStart()

    fun onStop()

    fun onResume()

    fun onPause()

    fun onSaveInstanceState(outState: Bundle)
}