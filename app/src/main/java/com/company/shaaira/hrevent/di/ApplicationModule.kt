package com.company.shaaira.hrevent.di

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(var context: Context) {

    @Provides
    @Singleton
    fun provideContext(): Context {
        return context
    }
}