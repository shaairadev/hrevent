package com.company.shaaira.hrevent.model.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.company.shaaira.hrevent.commondata.EventModel
import io.reactivex.Single

@Dao
interface EventsDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEvent(event: EventModel): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllEvents(events: List<EventModel>)

    @Query("SELECT * FROM eventmodel")
    fun getEventsList(): Single<List<EventModel>>


}