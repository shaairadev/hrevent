package com.company.shaaira.hrevent.model.datasource.local

import com.company.shaaira.hrevent.HRApp
import com.company.shaaira.hrevent.commondata.ParticipantModel
import com.company.shaaira.hrevent.model.database.dao.ParticipantsDAO
import io.reactivex.Single
import javax.inject.Inject

class ParticipantsRoomModule : ParticipantsLocalDatasource {

    init {
        HRApp.component.inject(this)
    }

    @Inject
    lateinit var participantsDAO: ParticipantsDAO


    override fun getParticipantsList(eventId: Int): Single<List<ParticipantModel>> {
        return participantsDAO.getParticipantsForEvent(eventId)
    }

    override fun updateParticipant(participant: ParticipantModel) {
        return participantsDAO.updateParticipant(participant)
    }
}