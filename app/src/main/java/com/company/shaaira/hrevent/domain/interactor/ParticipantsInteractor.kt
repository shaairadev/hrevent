package com.company.shaaira.hrevent.domain.interactor

import com.company.shaaira.hrevent.commondata.ParticipantModel
import com.company.shaaira.hrevent.model.ParticipantsRepository
import io.reactivex.Single

class ParticipantsInteractor{

    private val repository =  ParticipantsRepository()

    fun loadParticipantsList(eventId: Int): Single<List<ParticipantModel>> {
        return repository.loadParticipantsList(eventId)
    }

    fun updateParticipant(participant: ParticipantModel) {
        return repository.updateParticipant(participant)
    }
}