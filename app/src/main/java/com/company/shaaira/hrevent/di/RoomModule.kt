package com.company.shaaira.hrevent.di

import android.arch.persistence.room.Room
import android.content.Context
import com.company.shaaira.hrevent.model.database.ApplicationDatabase
import com.company.shaaira.hrevent.model.database.dao.EventsDAO
import com.company.shaaira.hrevent.model.database.dao.ParticipantsDAO
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {

    @Provides
    @Singleton
    fun provideEventsDAI(appDatabase: ApplicationDatabase): EventsDAO {
        return appDatabase.eventsDao()
    }

    @Provides
    @Singleton
    fun provideParticipantsDAO(appDatabase: ApplicationDatabase): ParticipantsDAO {
        return appDatabase.participantsDao()
    }

    @Provides
    @Singleton
    fun provideApplicationDatabase(context: Context): ApplicationDatabase {
        return Room.databaseBuilder(context, ApplicationDatabase::class.java, "database").build()
    }
}