package com.company.shaaira.hrevent.model

import com.company.shaaira.hrevent.commondata.ParticipantModel
import com.company.shaaira.hrevent.model.datasource.local.ParticipantsLocalDatasource
import com.company.shaaira.hrevent.model.datasource.local.ParticipantsRoomModule
import io.reactivex.Single

class ParticipantsRepository {

    private val localDataSource: ParticipantsLocalDatasource = ParticipantsRoomModule()

    fun loadParticipantsList(eventId: Int): Single<List<ParticipantModel>> {
        return localDataSource.getParticipantsList(eventId)
    }

    fun updateParticipant(participant: ParticipantModel) {
        localDataSource.updateParticipant(participant)
    }
}