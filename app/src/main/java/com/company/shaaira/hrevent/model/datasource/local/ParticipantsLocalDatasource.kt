package com.company.shaaira.hrevent.model.datasource.local

import com.company.shaaira.hrevent.commondata.ParticipantModel
import io.reactivex.Single

interface ParticipantsLocalDatasource {
    fun getParticipantsList(eventId: Int): Single<List<ParticipantModel>>
    fun updateParticipant(participant: ParticipantModel)
}