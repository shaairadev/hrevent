package com.company.shaaira.hrevent.model.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.company.shaaira.hrevent.commondata.EventModel
import com.company.shaaira.hrevent.commondata.ParticipantModel
import com.company.shaaira.hrevent.model.database.dao.EventsDAO
import com.company.shaaira.hrevent.model.database.dao.ParticipantsDAO

@Database(entities = [EventModel::class, ParticipantModel::class], version = 1, exportSchema = false)
abstract class ApplicationDatabase : RoomDatabase() {
    abstract fun eventsDao(): EventsDAO
    abstract fun participantsDao(): ParticipantsDAO
}