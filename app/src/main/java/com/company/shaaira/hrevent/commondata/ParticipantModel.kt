package com.company.shaaira.hrevent.commondata

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class ParticipantModel(

        @PrimaryKey
        val id: Int,
        val addition: String,
        val agreedByManager: String,
        val city: String,
        val company: String,
        val email: String,
        val firstName: String,
        val lastName: String,
        val patronymic: String,
        val phone: String,
        val position: String,
        val registeredDate: String,
        val eventId: Int,
        var isComing: Boolean = false
)