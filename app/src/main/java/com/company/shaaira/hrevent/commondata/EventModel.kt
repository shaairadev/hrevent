package com.company.shaaira.hrevent.commondata

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class EventModel(
    @PrimaryKey
    var id: Int = -1,
    var title: String = "",
    var description: String = "",
    var beginDate: Int = -1,
    var endDate: Int = -1,
    var cardImage: String = "",
    var totalMemberAmount: Int = 0,
    var cameMember: Int = 0
)