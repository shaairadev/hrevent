package com.company.shaaira.hrevent.extensions

import com.company.shaaira.hrevent.commondata.EventModel
import com.company.shaaira.hrevent.model.dto.EventDTO
import com.company.shaaira.hrevent.model.dto.ParticipantDTO
import java.text.SimpleDateFormat
import java.util.*

fun EventDTO.toEventEntity() = EventModel(
        id = this.id,
        title = this.title,
        description = this.description,
        beginDate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH).parse(this.date.start).time.toInt(),
        endDate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH).parse(this.date.end).time.toInt(),
        cardImage = this.cardImage
)

fun ParticipantDTO.toParticipantEntity(eventId: Int) = com.company.shaaira.hrevent.commondata.ParticipantModel(
        addition = this.addition,
        agreedByManager = this.agreedByManager,
        city = this.city,
        company = this.company,
        email = this.email,
        firstName = this.firstName,
        id = this.id,
        lastName = this.lastName,
        patronymic = this.patronymic,
        phone = this.phone,
        position = this.position,
        registeredDate = this.registeredDate,
        eventId = eventId
)