package com.company.shaaira.hrevent.presentation.participantsview

import android.os.Bundle
import android.util.Log
import com.company.shaaira.hrevent.commondata.ParticipantModel
import com.company.shaaira.hrevent.domain.interactor.ParticipantsInteractor
import com.company.shaaira.hrevent.lifecyclemodules.LifecycleListenerImpl
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class ParticipantsPresenter(val participantsView: ParticipantsView,
                            val eventId: Int)
    : LifecycleListenerImpl() {

    override fun onCreate(savedInstanceState: Bundle?) {
        getParticipants(eventId)
    }

    val disposable: CompositeDisposable = CompositeDisposable()
    private val participantsInteractor = ParticipantsInteractor()

    private fun getParticipants(eventId: Int) {
            disposable.addAll(participantsInteractor.loadParticipantsList(eventId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                participantsView.updateParticipantsList(it)
                            }, {

                    }
                    )
            )
    }

    fun updateParticipant(participantModel: ParticipantModel) {

        Completable.fromAction { participantsInteractor.updateParticipant(participantModel) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
    }
}