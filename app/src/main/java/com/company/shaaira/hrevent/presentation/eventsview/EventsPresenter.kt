package com.company.shaaira.hrevent.presentation.eventsview

import android.util.Log
import com.company.shaaira.hrevent.commondata.EventModel
import com.company.shaaira.hrevent.domain.interactor.EventsInteractor
import com.company.shaaira.hrevent.lifecyclemodules.LifecycleListenerImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class EventsPresenter(val eventsView: EventsView) : LifecycleListenerImpl() {

    private val interactor = EventsInteractor()

    lateinit var disposable: Disposable

    init {
        eventsView.setRefreshCallback(object : EventsView.RefreshCallback {
            override fun onRefresh() {
                getEvents()
            }
        })
    }

    fun getEvents() {
        disposable = interactor.loadEventsList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            Log.d("LENGTH", "Data length : " + it.size)
                            eventsView.updateEventList(it)
                            eventsView.setRefreshAnimation(false)
                        },
                        {}
                )
    }


    private fun setFakeEvents(): ArrayList<EventModel> {
        val list = ArrayList<EventModel>()
        list.apply {
            for (i: Int in 1..10) {
                add(EventModel())
            }
        }
        return list
    }

    override fun onStop() {
        disposable.dispose()
    }
}