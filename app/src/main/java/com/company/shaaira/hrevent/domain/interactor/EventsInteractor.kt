package com.company.shaaira.hrevent.domain.interactor

import com.company.shaaira.hrevent.commondata.EventModel
import com.company.shaaira.hrevent.model.EventsRepository
import io.reactivex.Single

class EventsInteractor {
    private val eventListRepository = EventsRepository()

    fun loadEventsList(): Single<List<EventModel>> {
        return eventListRepository.loadEventsList()
    }
}