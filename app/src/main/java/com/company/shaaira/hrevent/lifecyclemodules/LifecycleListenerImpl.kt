package com.company.shaaira.hrevent.lifecyclemodules

import android.os.Bundle
import android.os.PersistableBundle

abstract class LifecycleListenerImpl : LifecycleListener {

    override fun onCreate(savedInstanceState: Bundle?) {

    }

    override fun onPause() {

    }

    override fun onStop() {

    }

    override fun onStart() {

    }

    override fun onResume() {

    }

    override fun onSaveInstanceState(outState: Bundle) {

    }

    override fun onDestroy() {

    }
}
