package com.company.shaaira.hrevent

import android.app.Application
import android.util.Log
import com.company.shaaira.hrevent.di.*

class HRApp : Application() {
    companion object {
        const val baseUrl = "https://beta-team.cft.ru/"
        lateinit var component: ApplicationComponent
    }


    override fun onCreate() {
        super.onCreate()
        component = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .retrofitModule(RetrofitModule(baseUrl))
                .roomModule(RoomModule())
                .build()

    }
}