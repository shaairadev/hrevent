package com.company.shaaira.hrevent.model.datasource.network

import com.company.shaaira.hrevent.commondata.EventModel
import com.company.shaaira.hrevent.commondata.ParticipantModel
import io.reactivex.Single

interface EventsNetworkDatasource {
    fun loadEvents(): Single<List<EventModel>>
    fun loadParticipants(eventId: Int): Single<List<ParticipantModel>>
}