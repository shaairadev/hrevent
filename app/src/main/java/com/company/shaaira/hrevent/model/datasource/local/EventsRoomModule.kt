package com.company.shaaira.hrevent.model.datasource.local

import android.util.Log
import com.company.shaaira.hrevent.HRApp
import com.company.shaaira.hrevent.commondata.EventModel
import com.company.shaaira.hrevent.commondata.ParticipantModel
import com.company.shaaira.hrevent.model.database.dao.EventsDAO
import com.company.shaaira.hrevent.model.database.dao.ParticipantsDAO
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class EventsRoomModule : EventsLocalDatasource {

    init {
        HRApp.component.inject(this)
    }

    @Inject
    lateinit var eventsDAO: EventsDAO
    @Inject
    lateinit var participantsDAO: ParticipantsDAO

    override fun getEventsList(): Single<List<EventModel>> {
        return eventsDAO.getEventsList().flatMap { it ->
            Observable.fromIterable(it)
                    .map { event ->
                        participantsDAO.getParticipantsCount(event.id).subscribe({
                            event.totalMemberAmount = it
                            Log.i("total", it.toString())
                        }, { event.totalMemberAmount })
                        participantsDAO.getParticipantsCameCount(event.id).subscribe({ event.cameMember = it }, {})
                        event
                    }.toList()
        }
    }

    override fun saveEventsList(events: List<EventModel>) {
        return eventsDAO.insertAllEvents(events)
    }

    override fun saveParticipantsList(participants: List<ParticipantModel>) {
        return participantsDAO.insertParticipants(participants)
    }
}