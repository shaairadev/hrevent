package com.company.shaaira.hrevent.model.datasource.network

import com.company.shaaira.hrevent.HRApp
import com.company.shaaira.hrevent.commondata.EventModel
import com.company.shaaira.hrevent.commondata.ParticipantModel
import io.reactivex.Single
import javax.inject.Inject

class EventsNetworkModule : EventsNetworkDatasource {

    init {
        HRApp.component.inject(this)
    }

    @Inject
    lateinit var eventsAPIModule: EventsAPIModule

    override fun loadEvents(): Single<List<EventModel>> {
        return eventsAPIModule.getEventsCard()
    }

    override fun loadParticipants(eventId: Int): Single<List<ParticipantModel>> {
        return eventsAPIModule.getParticipants(eventId)
    }
}