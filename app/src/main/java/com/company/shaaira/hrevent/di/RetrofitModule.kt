package com.company.shaaira.hrevent.di

import com.company.shaaira.hrevent.model.CFTAPI
import com.company.shaaira.hrevent.model.datasource.network.EventsAPIModule
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class RetrofitModule(private var urlString: String) {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(urlString)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    fun provideEventAPIModule(retrofit: Retrofit): EventsAPIModule {
        val api = retrofit.create(CFTAPI::class.java)
        return EventsAPIModule(api)
    }
}